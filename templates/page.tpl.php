<?php
// $Id: page.tpl.php

/**
 * @file
 * Automobile's theme implementation to display a single Drupal page.
 *
 * Regions:
 * - $page['header_top']: Items for the header top region.
 * - $page['header_banner']: Items for the header banner region.
 * - $page['call_now']: Items for the call now region.
 * - $page['featured']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the first sidebar.
 * - $page['footer']: Items for the footer region.
 */
?>
<div id="outer">

  <div id="wrapper" class="clearfix">



    <div id="page_nav">
      <?php print render($page['header_top']); ?>
    </div> <!-- page nav #end -->

    <div id="header" class="clearfix">

      <?php if ($logo): ?>
        <div class="logo">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
          <?php if ($site_slogan): ?>
            <p class="blog-description">
              <?php print $site_slogan; ?>
            </p>
          <?php endif; ?>
        </div>
      <?php endif; ?>
     <!-- Header Banner -->
      <?php if ($page['header_banner']): ?>
        <?php print render($page['header_banner']); ?>
      <?php endif; ?>
     <!-- Call Now -->
      <?php if ($page['call_now']): ?>
        <?php print render($page['call_now']); ?>
      <?php endif; ?>
    </div> <!-- header #end -->

    <?php if ($main_menu): ?>
      <div id="categories_strip" class="categories_strip">
        <?php print theme('links__system_main_menu', array(
          'links' => $main_menu,
          'attributes' => array(
            'id' => 'main-menu-links',
            'class' => array('links', 'clearfix'),
          ),
          'heading' => array(
            'text' => t('Main menu'),
            'level' => 'h2',
            'class' => array('element-invisible'),
          ),
        )); ?>
      </div> <!-- /#main-menu -->
    <?php endif; ?>

    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar_l" class="sidebar">
        <?php print render($page['sidebar_first']); ?>
      </div> <!-- /.section, /#sidebar-first -->
    <?php endif; ?>

    <?php if ($page['featured']): ?>
      <?php print render($page['featured']); ?>
    <?php endif; ?>

    <div class="common_wrap">

        <div id="content">
          <?php if ($title): ?>
            <h1 class="title" id="page-title">
              <?php print $title; ?>
            </h1>
          <?php endif; ?>
          <?php if ($tabs): ?>
            <div class="tabs">
              <?php print render($tabs); ?>
            </div>
          <?php endif; ?>
          <?php print render($page['content']); ?>
        </div>

      <?php if ($page['sidebar_second']): ?>
        <div id="sidebar_r" class="sidebar">
          <?php print render($page['sidebar_second']); ?>
        </div> <!-- /.section, /#sidebar-second -->
      <?php endif; ?>
    </div>  <!--common_wrap #end -->

  </div><!-- wrapper #end -->

  <?php if ($page['footer']): ?>
    <?php print render($page['footer']); ?>
  <?php endif; ?>
  

</div> <!-- outer #end -->
